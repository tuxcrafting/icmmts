(function() {
    let link = document.createElement('a');
    link.href = URL.createObjectURL(
        new Blob(
            [JSON.stringify(pack)],
            {type: 'application/json'}));
    link.download = 'pack.json';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    delete link;
})();
