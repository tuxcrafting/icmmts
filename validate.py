#!/usr/bin/env python3

import libicmmts

pack = libicmmts.get_pack()
nations = libicmmts.get_nations()

blocks = {}
for k, n in nations.items():
    for b in n[1]:
        if b not in blocks:
            blocks[b] = set()
        blocks[b].add(k)

err = 0
for b, v in blocks.items():
    f = str(pack['cells']['feature'][b])
    if not pack['features'][f]['land']:
        err += 1
        print('%d: water claims: %s' % (b, ', '.join(v)))
    elif len(v) > 1:
        err += 1
        print('%d: overlapping claims: %s' % (b, ', '.join(v)))

print('%d errors' % err)
if err > 0:
    exit(1)
