import gzip
import hashlib
import json
import os
import struct

def get_pack():
    with gzip.open('pack.json.gz', 'rt') as f:
        return json.load(f)

def get_params():
    with open('params.json', 'r') as f:
        return json.load(f)

def gen_color(i):
    d = hashlib.md5(i.encode('utf-8')).digest()
    x, y = d[4], d[5]
    r = x
    g = (x - y) % 256
    b = (x + y) % 256
    return 'rgb(%d, %d, %d)' % (r, g, b)

def get_nations():
    files = []
    for x in os.listdir('nations'):
        if x.endswith('.txt'):
            files.append(x)
    files.sort()
    nations = {}
    for x in files:
        i = x[:-4]
        obj = ''
        cells = set()
        mode = 0
        with open('nations/%s' % x, 'r') as f:
            for l in f:
                ls = l.strip()
                if not ls or l[0] == '#':
                    continue
                if mode == 0:
                    if l == '%%\n':
                        mode = 1
                    else:
                        obj += l
                else:
                    cells.add(int(ls))
        nations[i] = (json.loads(obj), cells)

        if 'colour' not in nations[i][0]:
            nations[i][0]['colour'] = gen_color(i)

    return nations
